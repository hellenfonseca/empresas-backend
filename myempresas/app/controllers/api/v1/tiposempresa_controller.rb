module Api
    module V1
        class TiposempresaController < ApplicationController
            #before_action :authenticate_api_v1_user!
            def index
				tipos = Tipoempresa.order('id');
				render json: {status: 'SUCCESS', message:'Tipos empresa', data:tipos},status: :ok
            end
            def show
				tipo = Tipoempresa.find(params[:id]);
				render json: {status: 'SUCCESS', message:'Tipo Empresa by id', data:tipo},status: :ok
			end
        end
    end
end