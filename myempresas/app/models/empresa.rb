class Empresa < ApplicationRecord
  belongs_to :tipoempresa
  validates :nome, presence: true

  scope :tipoempresa, -> (tipoempresa,nome) {where(tipoempresa: tipoempresa) and where(nome: nome)}
  

end
