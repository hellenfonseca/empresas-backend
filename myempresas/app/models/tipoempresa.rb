class Tipoempresa < ApplicationRecord
    has_many :empresas 
    validates :tipo, presence: true
end
