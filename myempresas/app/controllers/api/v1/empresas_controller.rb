module Api
    module V1
        class EmpresasController < ApplicationController
            #before_action :authenticate_api_v1_user!
            def index      
                if params[:tipoempresa].present? and params[:nome].present?
                    empresa = Empresa.tipoempresa(params[:tipoempresa],params[:nome]);
                    render json: {status: 'SUCCESS', message:'Empresa by id and nome', data:empresa},status: :ok  
                    
                else
                    empresas = Empresa.order('id');
                    render json: {status: 'SUCCESS', message:'Empresas', data:empresas},status: :ok  
                end           
            end

            def show
				empresa = Empresa.find(params[:id]);
				render json: {status: 'SUCCESS', message:'Empresa by id', data:empresa},status: :ok
			end
        end
    end 
end