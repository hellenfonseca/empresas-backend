class CreateTipoempresas < ActiveRecord::Migration[5.2]
  def change
    create_table :tipoempresas do |t|
      t.string :tipo

      t.timestamps
    end
  end
end
