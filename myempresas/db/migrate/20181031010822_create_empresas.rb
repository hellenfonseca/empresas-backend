class CreateEmpresas < ActiveRecord::Migration[5.2]
  def change
    create_table :empresas do |t|
      t.string :nome
      t.belongs_to :tipoempresa, foreign_key: true

      t.timestamps
    end
  end
end
